let todoList = [];
let finishList = [];
let addTask = () => {
    let task = document.getElementById("newTask").value ;
    todoList.push(task);
    renderTask(todoList);
}
let renderTask = (todoList) => {
    let contentHTML = "";
    let index = -1;
    todoList.forEach(function(item){
        index++;
        contentHTML += `<li>${item}
        <div class="buttons">
        <button onclick="hoanThanhTask(${index})" class="complete"><i class="fa fa-check-circle"></i></button>
        <button onclick="xoaTask(${index})" class="remove"><i class="fa fa-trash"></i></button>
        </div>
        </li>`
        })
    document.getElementById("todo").innerHTML = contentHTML;
}
let xoaTask = (index) => {
    todoList.splice(index,1);
    renderTask(todoList);
    renderCompletedTask(finishList);
}
let hoanThanhTask = (index) => {
    let temp = todoList[index];
    finishList.push(temp);
    todoList.splice(index,1);
    renderTask(todoList);
    renderCompletedTask(finishList);
}
let renderCompletedTask = (finishList) => {
    let contentHTML = "";
    let index = -1;
    finishList.forEach(function(item){
        index++;
        contentHTML += `<li>${item}
        <div class="buttons">
        <button onclick="chuyenTask(${index})" class="complete"><i class="fa fa-check-circle"></i></button>
        <button onclick="xoaTask2(${index})" class="remove"><i class="fa fa-trash"></i></button>
        </div>
        </li>`
    })
    document.getElementById("completed").innerHTML = contentHTML;
}
let xoaTask2 = (index) => {
    finishList.splice(index,1);
    renderTask(todoList);
    renderCompletedTask(finishList);
}
let chuyenTask = (index) => {
    let temp = finishList[index];
    todoList.push(temp);
    finishList.splice(index,1);
    renderTask(todoList);
    renderCompletedTask(finishList);
}
let sapXepAZ = () => {
    todoList.sort();
    finishList.sort();
    renderTask(todoList);
    renderCompletedTask(finishList);
}
let sapXepZA = () => {
    todoList.sort();
    todoList.reverse();
    finishList.sort();
    finishList.reverse();
    renderTask(todoList);
    renderCompletedTask(finishList);
}